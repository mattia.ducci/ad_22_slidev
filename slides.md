<img src="/logoad.jpg" style="margin:auto; width:80%;" alt="accessibility days 2022"/>

---

# Scrivere il discorso

Con tempi molto serrati è utile scriversi il discorso
- Molto importante provare il discorso calcolando i tempi
- Evitare di ripetersi

---

# Buone pratiche per non annoiare

è fondamentale trovare strategie per evitare di annoiare il pubblico
- Dare il giusto ritmo
- Dare importanza alle pause

---

# L'importanza delle presentazioni

Le presentazioni debbono essere una guida per il discorso
- Punti essenziali per guidare il discorso
- Evitare di scrivere quello che dobbiamo dire a voce

---

# Mantenere il contatto con il pubblico

- Usare gentilezza
- Chiedere se ci sono domande

---

# Cosa fare e non fare

<table>
  <tr>
    <th>Cosa fare</th>
    <th>Cosa non fare</th> 
  </tr>
  <tr>
    <td>Creare una struttura del discorso</td>
    <td>Improvvisare</td> 
  </tr>
  <tr>
    <td>Usare un linguaggio semplice e fluido</td>
    <td>Leggere esclusivamente il testo delle slides</td> 
  </tr>
  <tr>
    <td>Fare buon uso del tono di voce</td>
    <td>Imparare a memoria il discorso</td> 
  </tr>
</table>